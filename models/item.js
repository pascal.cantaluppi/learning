require("custom-env").env();
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

mongoose.connect("mongodb://" + process.env.DB_HOST + "/catalog", {
  useCreateIndex: true,
  useNewUrlParser: true
});

var itemSchema = new Schema({
  itemId: { type: String, index: { unique: true } },
  itemName: String,
  price: Number,
  currency: String,
  categories: [String]
});

var item = mongoose.model("Item", itemSchema);

module.exports = { item: item };

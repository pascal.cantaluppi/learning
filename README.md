# Learning Node.js - RESTful Web APIs

Implementing a Web API providing routes for CRUD functions built with Node.js.

![API](https://gitlab.com/pascal.cantaluppi/learning/raw/master/img/js.png)

## Getting Started

This example shows how to implement a full fledged RESTful Service with Node.js and the express framework.

### Prerequisites

This project runs with Node.js, a JavaScript runtime environmen built on Chrome's V8 JavaScript engine.

[https://nodejs.org/](https://nodejs.org/)

### Installing

Clone this Git Repository, navigate into the created directory and install the dependencies using npm.

```
npm install
```

## Debugging

For debug mode use the `run dev` parameter.

```
npm run dev
```

## Running the tests

Testing with mock objects can be executed with the `test` command.

```
npm test
```

## Deployment

For deployment use the CI/CD tools supported by the cloud provider of your coice.

Please make shure that there is a `.env.production` on the server or use the environment variables available within your PaaS provider tools.

```
npm run
```

## Built With

- [Node](https://nodejs.org/) - JavaScript runtime environment
- [Express](https://expressjs.com/) - Fast, unopinionated, minimalist web framework
- [VS Code](https://code.visualstudio.com/) - Code Editing. Redefined
- :heart:

## Contributing

Please read the [Contributor Covenant](https://www.contributor-covenant.org/version/1/4/code-of-conduct.html) for details on my code of conduct, and the process for submitting pull requests to me.

## Versioning

I use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/pascal.cantaluppi/learning/-/tags).
Major releases are accessibla via url for backwards compoatibility `https://uri/v1/route`.

## Authors

- **Pascal Cantaluppi** - _Initial work_ - [@pascal.cantaluppi](https://gitlab.com/pascal.cantaluppi)

## License

This project is licensed under the MIT License - visit [opensource.org](https://opensource.org/licenses/MIT) for more informations.

## Acknowledgments

- Inspired by "RESTful Web API Desig witth Node.js 10, Third Edition" by Packt Publishing.

process.stdout.write("\033c");
require("custom-env").env();
var mongoose = require("mongoose");
var should = require("should");
var prepare = require("./prepare");

const item = require("../models/item").item;

mongoose.createConnection("mongodb://" + process.env.DB_HOST + "/catalog", {
  useCreateIndex: true,
  useNewUrlParser: true
});

describe("item: models", function() {
  describe("#create()", function() {
    it("Should create a new item", function(done) {
      var itemNew = {
        itemId: "1",
        itemName: "Sports Watch",
        price: 100,
        currency: "EUR",
        categories: ["Watches", "Sports Watches"]
      };

      item.create(itemNew, function(err, itemCreated) {
        // Check that no error occured
        should.not.exist(err);
        // Assert that the returned item has is what we expect
        itemCreated.itemId.should.equal("1");
        itemCreated.itemName.should.equal("Sports Watch");
        itemCreated.price.should.equal(100);
        itemCreated.currency.should.equal("EUR");
        itemCreated.categories[0].should.equal("Watches");
        itemCreated.categories[1].should.equal("Sports Watches");
        //Notify mocha that the test has completed
        done();
      });
    });
  });
});
